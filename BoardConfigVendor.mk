AB_OTA_PARTITIONS += \
    abl \
    aop \
    bluetooth \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    fsg \
    hyp \
    keymaster \
    logo \
    modem \
    prov \
    qupfw \
    storsec \
    tz \
    uefisecapp \
    xbl \
    xbl_config
